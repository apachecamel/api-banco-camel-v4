package com.accenture.spring.camel.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@Entity
@Table(name = "banco")
@XmlAccessorType(XmlAccessType.FIELD)
public class BancoModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@XmlAttribute(name = "name")
	private String name;
	
	@XmlAttribute(name = "clientes")
	private int clientes;
	
	@XmlAttribute(name = "recursos")
	private double recursos;
	
	public BancoModel(String name) {
		super();
		this.name = name;
		this.clientes = 0;
	}

	public BancoModel() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 
	 * @param saldo agrega el saldo a los recursos totales de la empresa, y ademas suma un cliente
	 */
	public void AgregarCuenta(double saldo) {
		
		this.clientes++;
		this.recursos = this.recursos + saldo;
	}

	public double getRecursos() {
		return recursos;
	}

	public void setRecursos(double recursos) {
		this.recursos = recursos;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getClientes() {
		return clientes;
	}

	public void setClientes(int clientes) {
		this.clientes = clientes;
	}
	
}
