package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.service.BancoService;

@Component
public class RemoveBankProcessor implements Processor {
	
	@Autowired
	private BancoService inMemoryBank;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		inMemoryBank.removeCuenta((Cuenta)exchange.getIn().getBody());
		
	}

}
