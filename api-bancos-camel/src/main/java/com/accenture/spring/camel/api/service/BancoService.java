package com.accenture.spring.camel.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.accenture.spring.camel.api.model.Cuenta;

@Service
public class BancoService {
	
	private List<Cuenta> list = new ArrayList<>();

	@PostConstruct
	public void initDB() {
	}

	public Cuenta addCuenta(Cuenta cuenta) {
		list.add(cuenta);
		return cuenta;
	}
	
	//borrar si se caga todo
	public void removeCuenta(Cuenta cuenta) {
		
		list.remove(cuenta);
		
	}

	public List<Cuenta> getInMemoryBank() {
		return list;
	}

}
