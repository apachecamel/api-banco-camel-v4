package com.accenture.spring.camel.api.resource;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangeTimedOutException;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.json.simple.JsonObject;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

/**
 * RouteBuilder que contiene la configuracion del componente rest, falta incluir todo esto en la abstracta.
 * 
 * 
 * @Authors ignacio.speicys , santiago.ferreiro
 *
 */
@Component
public class RestConfig extends ErrorHandlerRoute {
	

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		restConfiguration().component("servlet").port(9090).host("localhost").bindingMode(RestBindingMode.json)
		//Enable swagger endpoint.
		.apiContextPath("/swagger") //swagger endpoint path
		.apiContextRouteId("swagger") //id of route providing the swagger endpoint

		 //Swagger properties
		.contextPath("") //base.path swagger property; use the mapping path set for CamelServlet
		.apiProperty("api.title", "Cuenta de Bancos API")
		.apiProperty("api.version", "1.0");

	onException(ExchangeTimedOutException.class).process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				
				JsonObject obj = new JsonObject();
				
				obj.put("error", 1);
				obj.put("message", "fallo jdbc, se guardo la cuenta en /fail para uso futuro");
				exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 400);
				exchange.getOut().setBody(obj);
				
			}
		}).to("mock:catch6");

	}

}
