package com.accenture.spring.camel.api.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.accenture.spring.camel.api.model.Cuenta;

@Service
public class Resultados {
	
	private List<Cuenta> exitosos = new ArrayList<Cuenta>();
	private List<Cuenta> queue = new ArrayList<Cuenta>();
	private List<Cuenta> fallidos = new ArrayList<Cuenta>();
	
	public List<Cuenta> getExitosos() {
		return exitosos;
	}
	public void setExitosos(List<Cuenta> exitosos) {
		this.exitosos = exitosos;
	}
	public List<Cuenta> getQueue() {
		return queue;
	}
	public void setQueue(List<Cuenta> queue) {
		this.queue = queue;
	}
	public List<Cuenta> getFallidos() {
		return fallidos;
	}
	public void setFallidos(List<Cuenta> fallidos) {
		this.fallidos = fallidos;
	}
}
