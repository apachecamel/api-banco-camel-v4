package com.accenture.spring.camel.api.resource.builder;

import org.apache.cxf.message.MessageContentsList;

import io.spring.guides.gs_producing_web_service.Cuenta;
import io.spring.guides.gs_producing_web_service.GetCuentaResponse;

/**
 * El bean recibe un MessageContentsList que se debe acceder como lista conteniendo la informacion del
 * Response SOAP
 * 
 * @author ignacio.speicys , torrico.villaroel
 *
 */
public class GetCuentaResponseBuilder {
	
	public GetCuentaResponse getList(MessageContentsList list) {
		
		System.out.println(list.get(0));
		
		return (GetCuentaResponse)list.get(0);
	}

}
