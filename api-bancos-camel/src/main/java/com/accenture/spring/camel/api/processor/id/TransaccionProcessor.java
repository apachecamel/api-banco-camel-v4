package com.accenture.spring.camel.api.processor.id;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;
import com.accenture.spring.camel.api.exceptions.SaldoInsuficienteException;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.Transaccion;

@Component
public class TransaccionProcessor implements Processor {

	private Transaccion transaccion = new Transaccion();

	@Autowired
	private CuentaDao cuentadao;

	@Override
	public void process(Exchange exchange) throws Exception {

		JsonObject obj = new JsonObject();

		transaccion = exchange.getIn().getBody(Transaccion.class);

		System.out.println(transaccion.getMonto());

		Cuenta cuentaOrigen = cuentadao.findById(transaccion.getIdO()).orElse(null);

		Cuenta cuentaDestino = cuentadao.findById(transaccion.getIdD()).orElse(null);

			if (cuentaOrigen == null || cuentaDestino == null) {

				throw new CuentaNotFoundException();

			}

			if (cuentaOrigen.getSaldo() - transaccion.getMonto() < 0) {

				throw new SaldoInsuficienteException();

			} else {

				cuentaOrigen.setSaldo(cuentaOrigen.getSaldo() - transaccion.getMonto());

				cuentaDestino.setSaldo(cuentaDestino.getSaldo() + transaccion.getMonto());

				cuentadao.save(cuentaOrigen);

				cuentadao.save(cuentaDestino);

				obj.put("error", 0);
				obj.put("message",
						"Operacion exitosa entre " + cuentaOrigen.getName() + " y " + cuentaDestino.getName());
				exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
			}

		exchange.getOut().setBody(obj);

	}

}
