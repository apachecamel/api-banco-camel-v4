package com.accenture.spring.camel.api.resource.strategy;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.accenture.spring.camel.api.model.BancoModel;
import com.accenture.spring.camel.api.model.Cuenta;

/**
 * Aggregation strategy va a utilizar el header "banco" de body.banco como correlation Key,
 * ${body.banco} representa un string con nombres posibles: "galicia", "city", "nacion", pero podrian ser mas..
 * cada correlation key va a tener su propio oldExchange y eso proporciona una gran herramienta
 * para operar y agrupar la informacion como queramos.
 * 
 * @author ignacio.speicys
 *
 */
public class BancoAggregationStrategy implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		
		Cuenta cuenta = newExchange.getIn().getBody(Cuenta.class);
		
		if(oldExchange == null) {
			
			BancoModel banco = new BancoModel(cuenta.getBanco());
			
			banco.AgregarCuenta(cuenta.getSaldo());
			
			newExchange.getIn().setBody(banco);
			
			return newExchange;
			
		} else {
			
			BancoModel banco = oldExchange.getIn().getBody(BancoModel.class);
			
			banco.AgregarCuenta(cuenta.getSaldo());
			
			return oldExchange;
		}

	}

}
