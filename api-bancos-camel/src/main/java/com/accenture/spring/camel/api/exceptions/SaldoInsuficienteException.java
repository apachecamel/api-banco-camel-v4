package com.accenture.spring.camel.api.exceptions;

public class SaldoInsuficienteException extends AppException {
	
@Override
public void mostrarMensaje() {
		
		System.out.println("Error! Error! Error!");
		System.out.println("Saldo insuficiente");
	}

	@Override
	public String getMensaje() {
		
		return "Operacion invalida, no hay suficiente saldo en la cuenta";
	}

}
