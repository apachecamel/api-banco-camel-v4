package com.accenture.spring.camel.api.model;

public class Transaccion {
	
	private Long idO;
	private Long idD;
	private double monto;
	
	
	public Long getIdO() {
		return idO;
	}
	public void setIdO(Long idO) {
		this.idO = idO;
	}
	public Long getIdD() {
		return idD;
	}
	public void setIdD(Long idD) {
		this.idD = idD;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
}
