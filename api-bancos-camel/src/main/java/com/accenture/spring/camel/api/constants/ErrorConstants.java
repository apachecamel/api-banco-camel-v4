package com.accenture.spring.camel.api.constants;

public class ErrorConstants {
	
	//CODE:500
	public static final String BASE_CAIDA = "La base de datos se encuentra caida, guardamos la informacion del request para procesarla mas tarde";
	
	//CODE:406
	public static final String TRANSACCION_INVALIDA = "El monto ingresado no es valido";
	
	//CODE:404
	public static final String CUENTA_NOT_FOUND = "No se pudo encontrar la cuenta";
	
	//CODE:406
	public static final String SALDO_INSUFICIENTE = "La cuenta no posee suficiente saldo para realizar la extraccion";

	//CODE:500
	public static final String CATCH_ALL = "Hubo un error inesperado";
	

}
