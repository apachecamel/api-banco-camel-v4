package com.accenture.spring.camel.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.accenture.spring.camel.api.model.Cuenta;

public class CuentaList {
	
	private List<Cuenta> listaCuentas = new ArrayList<Cuenta>();

	public List<Cuenta> getListaCuentas() {
		return listaCuentas;
	}

	public void setListaCuentas(List<Cuenta> listaCuentas) {
		this.listaCuentas = listaCuentas;
	}
	
	

}
