package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.model.Cuenta;

@Component
public class ArchivoProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		Cuenta aux = new Cuenta();
		
		aux = exchange.getIn().getBody(Cuenta.class);
		
		
		if(aux.getCategoria() <= 1) {
			
			aux.setCategoria(1);
			
		}
		
		if(aux.getSaldo() < 0) {
			
			aux.setSaldo(0);
		}
		
		exchange.getOut().setBody(aux);
		
	}

}
