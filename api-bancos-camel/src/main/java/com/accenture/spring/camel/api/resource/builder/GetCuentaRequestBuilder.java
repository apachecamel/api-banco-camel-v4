package com.accenture.spring.camel.api.resource.builder;

import io.spring.guides.gs_producing_web_service.Cuenta;
import io.spring.guides.gs_producing_web_service.GetCuentaRequest;

/**
 * El bean recibe un nombre desde el exchange y lo carga en un Request que se va a enviar como SOAP (XML)
 * 
 * @author ignacio.speicys
 *
 */
public class GetCuentaRequestBuilder {
	
	public GetCuentaRequest getCuenta(String name) {
		
		GetCuentaRequest request = new GetCuentaRequest();
		
		request.setName(name);
		
		return request;
		
	}

}
