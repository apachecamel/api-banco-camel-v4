package com.accenture.spring.camel.api.constants;

/**
 * Repositorio central de todos los consumers, falta separar por producer/consumer 
 * en el caso de que se necesite configurar los endpoints en ambos casos.
 * 
 * @author ignacio.speicys , santiago.ferreiro
 *
 */
public class URI {
	
	public class Direct {	
		
		public class BDD {		
			public static final String IN = "direct:bdd";		
			public static final String PRE = "direct:prebdd";	
			public static final String FAIL = "direct:BDDCaida";
		}
		
		public class Marshall {
			public static final String ERROR = "direct:marshalError";
		}
		
		public class Error {
			public static final String CUENTA = "direct:cuentaNotFound";
			public static final String TRANSACCION = "direct:transaccionInvalida";
			public static final String SALDO_INSUFICIENTE = "direct:SaldoInsuficiente";
		}
	}
	public class Seda {
		public static final String CAT = "seda:categoria";
		public static final String BANK_QUEUE = "seda:bancoqueue";
		
		public class Test {
			public static final String BDD = "seda:bddtest";
		}
	}
	public class File {
		public static final String FAIL = "file:fail?preMove=inprogress&move=.done";
		public static final String DISPARADOR = "file:disparador";
		
		public class Aggregation {
			public static final String CSV = "file:aggregationCSV?noop=true&preMove=inprogress";
			public static final String JSON = "file:aggregationJSON?preMove=inprogress&move=../../";
		}
		public class Test {
			public static final String IN = "file:/test/entrada?move=../";
			public static final String IN_QUEUE = "file:/test/entradaqueue?move=../";
		}
	}
	public class Queue {
		public static final String BANK = "activemq:queue:banco";
		public static final String BACKUP = "activemq:queue:backup";
		public static final String LOAD_BALANCER = "activemq:queue:chequeoDeCarga";
		
		public class Test {
			public static final String BANK_2 = "activemq:queue:banco2";
		}
	}
	public class Mock {
		public static final String CATCH_ALL = "mock:catchAll";
		
	}


}
