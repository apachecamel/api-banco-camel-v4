package com.accenture.spring.camel.api.resource.rest;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestParamType;
import org.apache.cxf.message.MessageContentsList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.ErrorMessage;
import com.accenture.spring.camel.api.processor.CategoriaCustomProcessor;
import com.accenture.spring.camel.api.processor.id.GetCuentaByIdProcessor;
import com.accenture.spring.camel.api.resource.ErrorHandlerRoute;

import io.spring.guides.gs_producing_web_service.GetCuentaResponse;

/**
 * Contiene todas las rutas REST de tipo GET.
 * 
 * @Authors ignacio.speicys, santiago.ferreiro
 *
 */
@Component
public class GetResource extends ErrorHandlerRoute {

	@Autowired
	private CuentaDao cuentadao;

	@BeanInject
	private GetCuentaByIdProcessor procesadorid;
	
	@BeanInject
	private CategoriaCustomProcessor procesadorcustomcategoria;

	@Override
	public void configure() throws Exception {
		super.configure();

		rest("/cuenta")
		
			.get()
				.produces(MediaType.APPLICATION_JSON_VALUE)
				// SWAGGER
				.description("Recupera todas las cuentas")
				.responseMessage().code(200).responseModel(Cuenta.class).endResponseMessage()
				// RUTA
				.route()
					.setBody(() -> cuentadao.findAll())
			.endRest()
	
			.get("/{id}")
				.produces(MediaType.APPLICATION_JSON_VALUE)
				// SWAGGER
				.description("Recupera la cuentas del Id insertado")
				.param()
					.name("id")
					.type(RestParamType.path)
					.description("Id de la cuenta")
					.required(true).dataType("string")
				.endParam()
				.responseMessage().code(200).message("OK").responseModel(Cuenta.class).endResponseMessage() // OK
				.responseMessage().code(404).message("CUENTA NOT FOUND").responseModel(ErrorMessage.class).endResponseMessage() // Not-OK
				// RUTA
				.route()
					.doTry()
						.process(procesadorid)
					.endDoTry()
					.doCatch(CuentaNotFoundException.class).to("direct:cuentaNotFound")
			.endRest()
			
			.get("/custom")
				.produces(MediaType.APPLICATION_JSON_VALUE)
				.route()
					.setBody(() -> cuentadao.findAll())
					.split().simple("${body}")
						.process(procesadorcustomcategoria)
						.marshal().json(JsonLibrary.Jackson).log("${body}")
					.end()
			.endRest()
			
			.get("/LOGaderidos")
				.produces(MediaType.APPLICATION_JSON_VALUE)
				.route().routeId("aderidosROUTE")
					.setBody(()-> cuentadao.findAll())
					.split().body()
						.choice()
							.when().simple("${body.banco} not in 'galicia,city,nacion'")
								.log("${body.name} no esta en un banco aderido (${body.banco})")
						.end()
					.end()
			.endRest();
		
			/**
			 * Ejemplo para llamar un metodo de un bean configurado en Beans.xml 
			 */
			rest("/testingbean")
				.get().to("bean:DaoCuenta?method=findAll");
	}

}
