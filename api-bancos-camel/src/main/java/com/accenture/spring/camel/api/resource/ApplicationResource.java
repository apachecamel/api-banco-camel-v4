package com.accenture.spring.camel.api.resource;


import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.json.simple.JsonObject;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.transaction.CannotCreateTransactionException;

import com.accenture.spring.camel.api.constants.ErrorConstants;
import com.accenture.spring.camel.api.constants.URI;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.model.ErrorMessage;
import com.accenture.spring.camel.api.processor.BDDProcessor;
import com.accenture.spring.camel.api.processor.ArchivoProcessor;
import com.accenture.spring.camel.api.processor.log.LogearExitosos;
import com.accenture.spring.camel.api.processor.log.LogearFallidos;
import com.accenture.spring.camel.api.processor.log.LogearQueue;

//ruteador general que maneja todos los from(), no tiene rest
@Component
public class ApplicationResource extends ErrorHandlerRoute {
	
	@BeanInject
	private LogearQueue queue;
	
	@BeanInject
	private LogearFallidos fail;
	
	@BeanInject
	private LogearExitosos exito;
	
	@BeanInject
	private BDDProcessor processorBDD; 
	
	@Override
	public void configure() throws Exception {
		super.configure();
		
		/**
		 *  Procesa en el singleton el objeto y sigue su camino a la cola mediante un wiretap
		 */
		from(URI.Seda.CAT)
			.process(queue).marshal().json(JsonLibrary.Jackson, Cuenta.class)
			.wireTap(URI.Queue.BANK)
			;
			
		from("direct:fail")
			.process(fail);
		
		/**
		 *  Procesa en el singleton el objeto y envia al endpoint que impacta en base de datos
		 */
		from(URI.Direct.BDD.PRE)
			.process(exito).to(URI.Direct.BDD.IN);
		
		/**
		 *  Intenta impactar directo en BDD mediante el DAO, si falla la conexion, lo guarda en una Cola de backup
		 *  y muestra un mensaje de error.
		 */
		from(URI.Direct.BDD.IN)
			.log("intento entrar a BDD")
			.doTry()
				.process(processorBDD)
			.doCatch(CannotCreateTransactionException.class)
			.marshal().json(JsonLibrary.Jackson, Cuenta.class)
			.log("falla conexion, voy a la cola de backup")
			.to(URI.Queue.BACKUP)
			.to(URI.Direct.BDD.FAIL)
			.endDoTry();
		
		from(URI.Seda.Test.BDD)
			.log("entro a seda:bddtest")
			.process(processorBDD);

	}

}