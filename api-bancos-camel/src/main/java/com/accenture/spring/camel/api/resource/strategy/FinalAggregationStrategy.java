package com.accenture.spring.camel.api.resource.strategy;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.accenture.spring.camel.api.model.BancoCollection;
import com.accenture.spring.camel.api.model.BancoModel;

/**
 * 
 * Al aggregate van a entrar bancos uno a la vez unicamente y se van a mergear todos dentro de un mismo	
 * BancoCollection object.
 * 
 * @author ignacio.speicys
 *
 */
public class FinalAggregationStrategy implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange viejo, Exchange nuevo) {
		
		BancoModel banco = nuevo.getIn().getBody(BancoModel.class);
		
		System.out.println("Se esta agregando el banco "+banco.getName());
		
		if (viejo == null) {
			
			System.out.println("paso base por correlation key gracias a "+banco.getName());
			
			BancoCollection bancos = new BancoCollection();
			
			bancos.agregarBanco(banco);
			
			nuevo.getIn().setBody(bancos);
			
			return nuevo;
			
		} else {
			
			System.out.println("correlation key ya existente para "+banco.getName());
			
			BancoCollection bancos = viejo.getIn().getBody(BancoCollection.class);
			
			bancos.agregarBanco(banco);
			
			return viejo;
			
		}
		
	}

}
