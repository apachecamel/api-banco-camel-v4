package com.accenture.spring.camel.api.resource;

import java.util.Date;

import javax.xml.bind.JAXBContext;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.dataformat.soap.name.ServiceInterfaceStrategy;
import org.apache.camel.model.dataformat.BindyType;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.constants.URI;
import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.model.BancoCollection;
import com.accenture.spring.camel.api.model.BancoModel;
import com.accenture.spring.camel.api.model.Cuenta;
import com.accenture.spring.camel.api.processor.BDDProcessor;
import com.accenture.spring.camel.api.processor.BancoProcessor;
import com.accenture.spring.camel.api.processor.ArchivoProcessor;
import com.accenture.spring.camel.api.resource.builder.GetCuentaRequestBuilder;
import com.accenture.spring.camel.api.resource.builder.GetCuentaResponseBuilder;
import com.accenture.spring.camel.api.resource.strategy.BancoAggregationStrategy;
import com.accenture.spring.camel.api.resource.strategy.FinalAggregationStrategy;

/**
 * Contiene todas las rutas que parten desde carpetas/archivos del mismo directorio que el codebase.
 * 
 * @author ignacio.speicys
 *
 */
@Component
public class FileResource extends ErrorHandlerRoute {
	
	@BeanInject
	private BDDProcessor processorBDD;
	
	@BeanInject
	private ArchivoProcessor procesarArchivo;
	
	@BeanInject
	private BancoProcessor bancoprocesador;
	
	@Override
	public void configure() throws Exception {
		super.configure();
		
		
		from(URI.File.FAIL)
			.log("desde /fail, unmarsheo y mando hacia direct:bdd")
			.delay(1000*5)
			.unmarshal().json(JsonLibrary.Jackson, Cuenta.class)
			.to(URI.Direct.BDD.IN);
		
		from(URI.File.Test.IN)
			.unmarshal().json(JsonLibrary.Jackson, Cuenta.class)
			.process(procesarArchivo)
			.to("direct:bdd")
			.marshal().json(JsonLibrary.Jackson, Cuenta.class)
			.to("file:/test/historial");
		
		from(URI.File.Test.IN_QUEUE)
			.unmarshal().json(JsonLibrary.Jackson, Cuenta.class)
			.process(procesarArchivo)
			.marshal().json(JsonLibrary.Jackson, Cuenta.class)
			.to("activemq:queue:bancofile");
		
		/**
		 * Aggregation strategy va a utilizar el header "banco" de body.banco como correlation Key.
		 * body.banco representa un string con nombres posibles: "galicia", "city", "nacion"
		 * cada correlation key va a tener su propio oldExchange y eso proporciona una gran herramienta
		 * para operar y agrupar la informacion como queramos.
		 */
		
		from(URI.File.Aggregation.CSV)
			.unmarshal().bindy(BindyType.Csv, Cuenta.class)
			.split().body()
			.setHeader("banco", simple("${body.banco}"))
			.aggregate(header("banco"), new BancoAggregationStrategy())
			.completionTimeout(1000)
			.log("Nombre = ${body.name}, Clientes = ${body.clientes}, Recursos = ${body.recursos}");
		
		from("file:aggregationCSV/inprogress?move=../../")
			.log("ARCHIVO PROCESADO CON ESTATUS DESCONOCIDO, "
					+ "TENGA EL CUENTA QUE LA CARPETA DE INPUT SOLO ACEPTA .CSV");
		
		/**
		 * esta ruta simula un POST request con el mismo formato JSON, trabaja con un unmarshal custom de JacksonDataFormat
		*/
		from(URI.File.Aggregation.JSON)
			.unmarshal().json(JsonLibrary.Jackson, Cuenta[].class)
				.split().body()
				
					.process(processorBDD)
					
					.aggregate(simple("${body.banco}"), new BancoAggregationStrategy())
						.parallelProcessing()
						.completionTimeout(1000)
						
						.log("Nombre = ${body.name}, Clientes = ${body.clientes}, "
								+ "Recursos Totales = ${body.recursos}")					
						.process(bancoprocesador)
						
		/**
		 *  Una vez finalizado el aggregate, se abren X cantidad de rutas dado X correlation keys distintas
		 *  estas rutas se van a procesar de manera ordenada una vez que salen del aggregator en el orden
		 *  que se fueron creando los oldExchanges
		 *  
		 */
						.wireTap(URI.Seda.BANK_QUEUE)	
						
						
						.aggregate(constant(true), new FinalAggregationStrategy())
						.completionTimeout(1000)
						.log("A partir de aca, se unen todos los caminos del aggregate a uno solo..")
	
						.log("bod del XML mapeado de BancoCollection: ${body}")
						
						.to("file:logs/xml/"+new Date().getTime()+"?fileName=bancos.xml&fileExist=Override")
						.to("file:logs/xml?fileName=unixml.xml&fileExist=Append")
						.log("se agregaron los bancos en un XML, tarea finalizada..")
						;
		
		
		from(URI.Seda.BANK_QUEUE).delay(2000)
		.setHeader("recursos", simple("${body.recursos}"))
		.marshal().json(JsonLibrary.Jackson, BancoModel.class)
		.log("log 1 orden original ${body}")
		
		/**
		 *  por default, el resequencing va a ser en batch, es decir, va a recolectar todos los mensajes 
		 *  y luego procesar el output ordenado despues del timeout establecido como configuracion
		 */
		.resequence(header("recursos")).batch().timeout(5*1000)
		.log("log 2 ordenado ${body}");
		
		/**
		 *  test para probar el request/response SOAP, se usa la carpeta testSOAP como disparador, pero 
		 *  el body se hardcodea como "Ignacio" y pega en el WS.
		 */
		
		from("file:testSOAP?preMove=../&move=../")
		.setBody(constant("Ignacio"))
			.bean(GetCuentaRequestBuilder.class) //esto lo transforma en XML
				.log("${body}")
				.log("termino de llamar al bean..")
			.setHeader(CxfConstants.OPERATION_NAME, constant("getCuenta"))
			.setHeader(CxfConstants.OPERATION_NAMESPACE, 
					constant("http://spring.io/guides/gs-producing-web-service"))	
			.to("cxf:bean:soapDesdeBean")
			.bean(GetCuentaResponseBuilder.class)
			.log("body final: ${body}");
	}

}
