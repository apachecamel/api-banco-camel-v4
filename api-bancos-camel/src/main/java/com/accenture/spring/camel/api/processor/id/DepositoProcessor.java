package com.accenture.spring.camel.api.processor.id;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dao.CuentaDao;
import com.accenture.spring.camel.api.exceptions.CuentaNotFoundException;
import com.accenture.spring.camel.api.exceptions.SaldoInsuficienteException;
import com.accenture.spring.camel.api.model.Cuenta;

@Component
public class DepositoProcessor implements Processor {

	@Autowired
	private CuentaDao cuentadao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// creo una cuenta que alvergue los pocos datos que vienen (id + deposito)
		Cuenta deposito = exchange.getIn().getBody(Cuenta.class);

		// creo una cuenta auxiliar y le asigno la cuenta guardado por id en bdd
		Cuenta banco1 = cuentadao.findById(deposito.getId()).orElse(null);
		JsonObject j = new JsonObject();

			if (banco1 == null) {

				throw (new CuentaNotFoundException());

			} else {

				// hago un merge de las 2 variables, quedandome con el deposito nuevo, no toco
				// id
				banco1.setSaldo(banco1.getSaldo() + deposito.getSaldo());

				// impacto la cuenta modificada en bdd.
				cuentadao.save(banco1);

				// finalmente al exchange se le asigna el nuevo cuerpo de la cuenta actualizada.
				exchange.getOut().setBody(banco1);
			}

	}
}
